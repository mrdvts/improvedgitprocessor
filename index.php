<?php
require_once(__DIR__.'/vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Processor\GitProcessor;

define('ROOT', dirname(__DIR__));

class Git
{
    /** @var string Project root directory */
    protected $root;

    /** @var string[]|null[] File cache */
    private static $fileCache = [];

    public function __construct(string $root = __DIR__)
    {
        $this->root = $root;
    }

    /**
     * Получить название текущей ветки
     *
     * @param bool $short
     *
     * @return string|null
     */
    public function getBranch(): ?string
    {
        $branch = null;
        $head = $this->getFile('HEAD');

        if (null !== $head && 1 === preg_match('/^ref: refs\/heads\/(.*)$/', $head, $refs)) {
            $branch = $refs[1];
        }

        return $branch;
    }

    /**
     * Получить хеш коммита.
     *
     * @param bool $short
     *
     * @return string|null
     */
    public function getHash(bool $short = false): ?string
    {
        $hash = null;
        $head = $this->getFile('HEAD');

        if (null !== $head && 1 === preg_match('/^ref: (.*)$/', $head, $refs)) {
            $hash = $this->getFile($refs[1]);

            if (null !== $hash) {
                $hash = trim($hash);
                $hash = $short ? substr($hash, 0, 8) : $hash;
            }
        }


        return $hash;
    }

    /**
     * Получить какой-то гитовый файл
     *
     * @param string $filename
     * @return null|string
     */
    private function getFile(string $filename): ?string
    {
        $filename = $this->root . '/.git/' . $filename;

        if (!array_key_exists($filename, self::$fileCache)) {
            self::$fileCache[$filename] = null;

            if (file_exists($filename)) {
                $contents = file_get_contents($filename);
                if (false === $contents) {
                    $contents = null;
                }
                self::$fileCache[$filename] = $contents;
            }
        }

        return self::$fileCache[$filename];
    }
}



class ImprovedGitProcessor extends GitProcessor
{
    /** @var Git */
    protected static $git;

    public function __construct(int $level = Logger::DEBUG)
    {
        parent::__construct($level);
        static::$git = new Git(ROOT);
    }

    private static function getGitInfo(): array
    {
        return [
            'branch' => static::$git->getBranch(),
            'commit' => static::$git->getHash(),
        ];
    }
}

// create a log channel
$log = new Logger('foo');

$log->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$log->pushProcessor(new ImprovedGitProcessor());

$log->warning('bar');
