ImprovedGitProcessor,

отличие от [дефолтного](https://github.com/Seldaek/monolog/blob/master/src/Monolog/Processor/GitProcessor.php) 
в том, что не порождает новые процессы

```bash
$ git branch -v --no-abbrev
```

